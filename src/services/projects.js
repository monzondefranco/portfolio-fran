const projects = [
    {
        id: 1,
        image: {
        src: '',
        alt: 'Blue Rabbit',
        },
        description: [
        'Mobile application using React Native.',
        'Connection to Firebase for push notifications.',
        ],
        link: {
        href: 'https://es.bluerabbit.co/',
        isDisabled: false,
        textLink: 'es.bluerabbit.co/',
        }
    }
]

export default projects