import React, { useState } from 'react';
import { Link } from 'react-router-dom';

import { Container, Menu, MenuItem, ResponsiveButton } from './styles';
import { AiOutlineMenu, AiOutlineClose } from 'react-icons/ai';

import Drawer from './drawer';
import { DefaultButton } from '../button';



const Header = () => {
  // responsive navbar status
const [navbarStatus, setNavbarStatus] = useState(false);

return (
    <Container>
        <Link
            to="/"
            style={{
                color: `#141c3a`,
                textDecoration: `none`,
                zIndex: 3,
            }}
            >
            <img
                style={{ margin: 0 }}
                src="/black_logo.png"
                alt="DS Logo"
                width="150px"
            />
        </Link>
        <Menu>
        <Link
            to="/about"
            style={{
                color: `#141c3a`,
                textDecoration: `none`,
            }}
        >
        <MenuItem>About me</MenuItem>
        </Link>
        <Link
            to="/contact"
            style={{
                color: `#141c3a`,
                textDecoration: `none`,
            }}
        >
        <MenuItem>
            <DefaultButton value="Get in touch" />
        </MenuItem>
        </Link>
        </Menu>

    <ResponsiveButton onClick={() => setNavbarStatus(!navbarStatus)}>
        {navbarStatus ? <AiOutlineClose /> : <AiOutlineMenu />}
    </ResponsiveButton>

    <Drawer isOpen={navbarStatus} />
    </Container>
    );
};

export default Header;